package com.example.timudal;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.example.timudal.controller.TibudalController.getRandomList;
import static com.example.timudal.controller.TibudalController.shuffle;

@SpringBootTest
class TimudalApplicationTests {

    @Test
    void contextLoads() {
//        System.out.println(shuffle(getRandomList()).toString());
//        String type = "";
//        System.out.println((type == null || type.isEmpty()));
        List<Integer> list = new ArrayList<>();
        list.add(100);
        list.add(1);
        list.add(5);
        list.add(20);
        Collections.sort(list);
        System.out.println(list.toString());
    }
}
