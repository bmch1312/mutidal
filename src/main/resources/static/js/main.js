'use strict';

var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');

var stompClient = null;
var username = null;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

function connect(event) {
    username = document.querySelector('#name').value.trim();

    if (username) {
        usernamePage.classList.add('hidden');
        chatPage.classList.remove('hidden');

        var socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);

        stompClient.connect({}, onConnected, onError);
    }
    event.preventDefault();
}

function onConnected() {
    // Subscribe to the Public Topic
    stompClient.subscribe('/topic/chat', onChatReceived);
    stompClient.subscribe('/topic/base', onBaseReceived);

    var message = {
        messageType: "JOIN"
        , data: {
            userName: username
            , hostYn: 'N'
        }
    };
    // Tell your username to the server
    stompClient.send("/app/base/user/add",
        {},
        JSON.stringify(message)
    );


    connectingElement.classList.add('hidden');
}


function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}


var hostYn = '';

function sendMessage(event) {
    var messageContent = messageInput.value.trim();
    if (messageContent && stompClient) {
        var message = {
            messageType: 'CHAT',
            data: {
                content: messageInput.value,
                user: {
                    userName: username,
                    // 저장된 호스트로 사용
                    hostYn: hostYn === '' ? 'N' : hostYn
                }
            }
        };

        stompClient.send("/app/chat/send",
            {},
            JSON.stringify(message));
        messageInput.value = '';
    }
    event.preventDefault();
}

function onBaseReceived(payload) {
    var message = JSON.parse(payload.body);
    var data = message.data;
    if (message.messageType === 'LEAVE' || message.messageType === 'JOIN') {
        var messageElement = document.createElement('li');
        var content;
        if (message.messageType === 'JOIN') {
            messageElement.classList.add('event-message');
            content = data.userName + ' joined!';
            // 호스트 저장
            hostYn = data.hostYn;

        } else if (message.messageType === 'LEAVE') {
            messageElement.classList.add('event-message');
            content = data.userName + ' left!';
        }
        var textElement = document.createElement('p');
        var messageText = document.createTextNode(content);
        textElement.appendChild(messageText);
        messageElement.appendChild(textElement);
        messageArea.appendChild(messageElement);
        messageArea.scrollTop = messageArea.scrollHeight;
    }
}

function onChatReceived(payload) {
    var message = JSON.parse(payload.body);
    var data = message.data;
    var user = message.data.user;
    var messageElement = document.createElement('li');
    messageElement.classList.add('chat-message');

    var avatarElement = document.createElement('i');
    var avatarText = document.createTextNode(user.userName[0]);
    avatarElement.appendChild(avatarText);
    avatarElement.style['background-color'] = getAvatarColor(user.userName);

    messageElement.appendChild(avatarElement);

    // 이름 붙이는 span태그
    // var usernameElement = document.createElement('span');
    // var usernameText = document.createTextNode(user.userName);
    // usernameElement.appendChild(usernameText);
    // messageElement.appendChild(usernameElement);

    // 내용 p태그
    var textElement = document.createElement('p');
    var messageText = document.createTextNode(data.content);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}


function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }
    var index = Math.abs(hash % colors.length);
    return colors[index];
}

usernameForm.addEventListener('submit', connect, true)
messageForm.addEventListener('submit', sendMessage, true)