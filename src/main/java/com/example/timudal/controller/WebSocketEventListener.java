package com.example.timudal.controller;

import com.example.timudal.TimudalApplication;
import com.example.timudal.model.Common;
import com.example.timudal.model.Message;
import com.example.timudal.model.MessageType;
import com.example.timudal.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;


@Component
public class WebSocketEventListener {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);
    private Common common = TimudalApplication.common;

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        logger.info("Received a new web socket connection");
        common.setUserCount(common.getUserCount() + 1);
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());

        String username = (String) headerAccessor.getSessionAttributes().get("username");
        if (username != null) {
            common.setUserCount(common.getUserCount() - 1);
            logger.info("User Disconnected : " + username);
            Message<User> message = new Message<>();
            User user = new User();
            user.setUserName(username);
            message.setMessageType(MessageType.LEAVE);
            message.setData(user);
            messagingTemplate.convertAndSend("/topic/base", message);
        }
    }
}