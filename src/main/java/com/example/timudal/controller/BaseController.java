package com.example.timudal.controller;

import com.example.timudal.TimudalApplication;
import com.example.timudal.model.Common;
import com.example.timudal.model.Message;
import com.example.timudal.model.User;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

@Controller
public class BaseController {

    private Common common = TimudalApplication.common;

    @MessageMapping("/base/user/add")
    @SendTo("/topic/base")
    public Message<User> addUser(@Payload Message<User> message, SimpMessageHeaderAccessor headerAccessor){
        if (common.getStatus().equals("PLAY")){
            message.setStatus("FAIL");
            message.setMsg("이미 시작되어 참가할 수 없습니다");
        }
        headerAccessor.getSessionAttributes().put("username", message.getData().getUserName());
        if (common.getHostUser().isEmpty()){
            common.setHostUser(message.getData().getUserName());
            message.getData().setHostYn("Y");
        }
        message.setStatus("SUCCESS");
        message.setMsg("성공");
        return message;
    }
}
