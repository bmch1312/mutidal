package com.example.timudal.controller;

import com.example.timudal.TimudalApplication;
import com.example.timudal.model.Common;
import com.example.timudal.model.Message;
import com.example.timudal.model.MessageType;
import com.example.timudal.model.Timudal;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/app")
public class TibudalRestController {

    private Common common = TimudalApplication.common;

    /**
     * 무티 초기화
     */
    @RequestMapping(value = "/timudal/init", method = {RequestMethod.POST})
    public Message<Timudal> setTimudalInit(@RequestBody Message<Timudal> message) {
        //Test
        common.setUserCount(3);
        common.setHostUser("bin");
        if (!message.getData().getUserName().equals(common.getHostUser())) {
            message.setStatus("FAIL");
            message.setMsg("호스트가 아닙니다");
            return message;
        }
        if (common.getUserCount() < 2) {
            message.setStatus("FAIL");
            message.setMsg("최소 인원이 부족합니다");
            return message;
        }
        if (common.getUserCount() > 9) {
            message.setStatus("FAIL");
            message.setMsg("최대 인원을 초과하였습니다");
            return message;
        }
        init();
        message.setStatus("SUCCESS");
        message.setMsg("성공");
        return message;
    }

    /**
     * 번호 뽑기 SELECT / 순위정해주기 SELECT_DONE
     */
    @RequestMapping(value = "/timudal/select", method = {RequestMethod.POST})
    public Message<Timudal> setTimudalSelect(@RequestBody Message<Timudal> message) {
        if (message.getMessageType() == MessageType.SELECT) {
            //번호뽑기
            if (message.getData().getInitNum() != null && !message.getData().getInitNum().isEmpty()) {
                message.setStatus("FAIL");
                message.setMsg("이미 번호를 선택하셨습니다. 잠시만 기다려주세요");
                return message;
            }
            if (message.getData().getSelectNum() == null || message.getData().getSelectNum().isEmpty()) {
                message.setStatus("FAIL");
                message.setMsg("선택한 번호가 없습니다. 다시 선택해주세요");
                return message;
            }
            if (common.getSelectNumList().contains(message.getData().getSelectNum())) {
                message.setStatus("FAIL");
                message.setMsg("이미 선택된 번호 입니다. 다시 골라주세요");
                return message;
            }
            int selectNum = Integer.parseInt(message.getData().getSelectNum());
            String initNum = common.getRandomNumList().get(selectNum);
            common.getRandomNumList().remove(selectNum);
            message.getData().setInitNum(initNum);
            common.getSelectNumList().add(message.getData().getSelectNum());
            common.getInitNumList().add(Integer.parseInt(initNum));
            if (common.getUserCount() == common.getSelectNumList().size()) {
                message.setMessageType(MessageType.SELECT_DONE);
            }
        } else if (message.getMessageType() == MessageType.SELECT_DONE) {
            message.setMessageType(MessageType.TRIBUTE_CONFIRM);
            //순위
            Collections.sort(common.getInitNumList());
            for (int i = 0; i <= common.getInitNumList().size(); i++) {
                if (message.getData().getInitNum().equals(String.valueOf(common.getInitNumList().get(i)))) {
                    message.getData().setMyRank(i + 1);
                    break;
                }
            }
            //Deck
            List<String> baseDeck = common.getBaseDeck();
            int deckSize = common.getBaseDeckSize() / common.getUserCount();
            if (deckSize > 20) {
                deckSize = 20;
            }
            List<String> userDeck = new ArrayList<>();
            for (int i = deckSize; i > 0; i--) {
                int ran = (int) (Math.random() * baseDeck.size());
                userDeck.add(baseDeck.get(ran));
                baseDeck.remove(ran);
            }
            userDeck.sort(Comparator.comparingInt(Integer::parseInt));
            common.getUserDeckMap().put(message.getData().getMyRank(), userDeck);
            message.getData().setDeckList(userDeck);

        } else {
            message.setStatus("FAIL");
            message.setMsg("Request Error");
            return message;
        }
        message.setStatus("SUCCESS");
        message.setMsg("성공");
        return message;
    }

    /**
     * 조공
     */
    @RequestMapping(value = "/timudal/tribute", method = {RequestMethod.POST})
    public Message<Timudal> setTimudalTribute(@RequestBody Message<Timudal> message) {
        if (message.getMessageType() == MessageType.TRIBUTE_CONFIRM) {
            // 확인 및 배치
            int userCnt = common.getUserCount();
            int myRank = message.getData().getMyRank();
            int tributeRank = message.getData().getTributeRank();
            Map<Integer, List<String>> userDeckMap = common.getUserDeckMap();
            if (tributeRank != 0) {
                // X
                List<String> tributeList = common.getUserDeckMap().get(tributeRank);
                String confirm = tributeList.get(tributeList.size() - 1);
                String confirm2 = tributeList.get(tributeList.size() - 2);
                if ("13".equals(confirm) && "13".equals(confirm2)) {
                    if (tributeRank == userCnt) {
                        //순서반대
                        message.getData().setMyRank(userCnt - myRank + 1);
                    }
                    message.setMessageType(MessageType.PLAY);
                } else {
                    message.setStatus("FAIL");
                    message.setMsg("조커 두장이 아닙니다.");
                    message.setMessageType(MessageType.TRIBUTE);
                    return message;
                }
            } else {
                // O
                List<String> lastDeck = userDeckMap.get(userCnt);
                message.setMessageType(MessageType.TRIBUTE);
                if (userCnt <= 3) {
                    if (myRank == 1) {
                        message.getData().getDeckList().add(lastDeck.get(0));
                        message.getData().getDeckList().add(lastDeck.get(1));
                    }
                    if (myRank == userCnt) {
                        message.getData().getDeckList().remove(0);
                        message.getData().getDeckList().remove(0);
                    }
                } else {
                    if (myRank == 1) {
                        message.getData().getDeckList().add(lastDeck.get(0));
                        message.getData().getDeckList().add(lastDeck.get(1));
                    }
                    if (myRank == 2) {
                        List<String> secondLastDeck = userDeckMap.get(userCnt - 1);
                        message.getData().getDeckList().add(secondLastDeck.get(0));
                    }
                    if (myRank == userCnt) {
                        message.getData().getDeckList().remove(0);
                        message.getData().getDeckList().remove(0);
                    }
                    if (myRank == userCnt - 1) {
                        message.getData().getDeckList().remove(0);
                    }
                }
                message.getData().getDeckList().sort(Comparator.comparingInt(Integer::parseInt));
                userDeckMap.put(myRank, message.getData().getDeckList());
            }
        } else if (message.getMessageType() == MessageType.TRIBUTE) {
            // 전달
            int userCnt = common.getUserCount();
            int myRank = message.getData().getMyRank();
            int tributeCount = common.getTributeCount();
            Map<Integer, List<String>> userDeckMap = common.getUserDeckMap();
            List<String> changeDeckList = message.getData().getChangeDeckList();
            if (userCnt <= 3) {
                if (myRank == 1) {
                    userDeckMap.get(userCnt).addAll(changeDeckList);
                    userDeckMap.get(userCnt).sort(Comparator.comparingInt(Integer::parseInt));
                    userDeckMap.put(userCnt, message.getData().getDeckList());
                    userDeckMap.put(1, message.getData().getDeckList());
                }
                message.getData().setDeckList(userDeckMap.get(myRank));
                message.setMessageType(MessageType.TRIBUTE_DONE);
            } else {
                if (myRank == 1) {
                    userDeckMap.get(userCnt).addAll(changeDeckList);
                    userDeckMap.get(userCnt).sort(Comparator.comparingInt(Integer::parseInt));
                    userDeckMap.put(userCnt, message.getData().getDeckList());
                    userDeckMap.put(1, message.getData().getDeckList());
                    tributeCount++;
                }
                if (myRank == 2) {
                    userDeckMap.get(userCnt - 1).addAll(changeDeckList);
                    userDeckMap.get(userCnt - 1).sort(Comparator.comparingInt(Integer::parseInt));
                    userDeckMap.put(userCnt - 1, message.getData().getDeckList());
                    userDeckMap.put(1, message.getData().getDeckList());
                    tributeCount++;
                }
                common.setTributeCount(tributeCount);
                message.getData().setDeckList(userDeckMap.get(myRank));
                if (tributeCount == 2){
                    message.setMessageType(MessageType.TRIBUTE_DONE);
                }
            }
        } else if (message.getMessageType() == MessageType.TRIBUTE_DONE) {
            message.getData().setDeckList(common.getUserDeckMap().get(message.getData().getMyRank()));
            message.setMessageType(MessageType.PLAY);
        } else {
            message.setStatus("FAIL");
            message.setMsg("Request Error");
            return message;
        }
        message.setStatus("SUCCESS");
        message.setMsg("성공");
        return message;
    }

    /**
     * 진행
     */
    @RequestMapping(value = "/timudal/play", method = {RequestMethod.POST})
    public Message<Timudal> setTimudalPlay(@RequestBody Message<Timudal> message) {
        int userCnt = common.getUserCount();
        int nowTurn = common.getNowTurn();
        int turnCount = common.getTurnCount();
        int finishPlayerCount = common.getFinishPlayerCount();
        Map<Integer, List<String>> userDeckMap = common.getUserDeckMap();
        List<String> deckList = message.getData().getDeckList();
        List<String> beforeDeal = common.getBeforeDeal();
        List<String> dealDeckList = message.getData().getDealDeckList();
        if (finishPlayerCount == userCnt) {
            message.setStatus("FAIL");
            message.setMsg("이미 종료된 판입니다");
            return message;
        }
        if (deckList.size() == 0) {
            message.setStatus("FAIL");
            message.setMsg("모든 패를 내셨습니다");
            return message;
        }
        if (message.getMessageType() == MessageType.DEAL) {
            //DEAL
            if (dealDeckList.size() == 0) {
                message.setStatus("FAIL");
                message.setMsg("덱을 제시하지 않았습니다");
                return message;
            }
            if (dealDeckList.size() > 1) {
                // 제시한 덱의 숫자가 다 같은지
                String tempCard = "";
                for (String s : dealDeckList) {
                    if (tempCard.isEmpty()) {
                        tempCard = s;
                    }
                    if (!tempCard.equals(s)) {
                        message.setStatus("FAIL");
                        message.setMsg("여러장의 카드가 같지 않습니다");
                        return message;
                    }
                }
            }
            if (beforeDeal.size() > 0 && beforeDeal.size() != dealDeckList.size()) {
                message.setStatus("FAIL");
                message.setMsg("이전 차례의 갯수와 일치하지 않습니다");
                return message;
            }
            if (beforeDeal.size() > 0) {
                int beforeDeckNum = Integer.parseInt(beforeDeal.get(0));
                int dealDeckNum = Integer.parseInt(dealDeckList.get(0));
                if (beforeDeckNum <= dealDeckNum) {
                    message.setStatus("FAIL");
                    message.setMsg("이전 차례보다 작은 숫자를 내주세요");
                    return message;
                }
            }
            // 통과
            common.setTurnCount(0);
            for (String s : dealDeckList) {
                deckList.remove(s);
            }
            userDeckMap.put(message.getData().getMyRank(), deckList);
            beforeDeal.clear();
            beforeDeal.addAll(dealDeckList);
            if (deckList.size() == 0) {
                // 종료된 User처리
                finishPlayerCount++;
                common.setFinishPlayerCount(finishPlayerCount);
                message.getData().setFinishPlayerCount(finishPlayerCount);
                message.getData().setMyRank(finishPlayerCount);
                common.setTurnCount(-1);
            }
            // 다음 턴 처리
            if (nowTurn == (userCnt - finishPlayerCount)) {
                message.getData().setNowTurn(1);
                common.setNowTurn(1);
            } else {
                nowTurn++;
                message.getData().setNowTurn(nowTurn);
                common.setNowTurn(nowTurn);
            }
            message.setStatus("SUCCESS");
            message.setMsg("성공");
            return message;
        } else if (message.getMessageType() == MessageType.PASS) {
            //PASS
            turnCount++;
            if (nowTurn == (userCnt - finishPlayerCount)) {
                message.getData().setNowTurn(1);
                common.setNowTurn(1);
            } else {
                nowTurn++;
                message.getData().setNowTurn(nowTurn);
                common.setNowTurn(nowTurn);
            }
            common.setTurnCount(turnCount);
            if (turnCount == (userCnt - 1 - finishPlayerCount)) {
                beforeDeal.clear();
                common.setTurnCount(0);
            }
            message.setStatus("SUCCESS");
            message.setMsg("성공");
            return message;
        } else {
            message.setStatus("FAIL");
            message.setMsg("Request Error");
            return message;
        }
    }

    private void init() {
        common.setTurnCount(0);
        common.setNowTurn(1);
        common.setFinishPlayerCount(0);
        common.setTributeCount(0);
        common.getBeforeDeal().clear();
        common.setBaseDeck(getBaseDeck());
        common.getSelectNumList().clear();
        common.setRandomNumList(shuffle(getRandomList()));
        common.getInitNumList().clear();
        common.getUserDeckMap().clear();
    }

    /**
     * 기본덱
     */
    private static List<String> getBaseDeck() {
        List<String> baseDeck = new ArrayList<>();
        baseDeck.add("13");
        baseDeck.add("13");
        for (int i = 1; i < 13; i++) {
            for (int j = 1; j <= i; j++) {
                baseDeck.add(String.valueOf(i));
            }
        }
        return baseDeck;
    }

    /**
     * 랜덤번호
     */
    public static List<String> getRandomList() {
        List<String> randomNumList = new ArrayList<>();
        for (int i = 1; i < 81; i++) {
            randomNumList.add(String.valueOf(i));
        }
        return randomNumList;
    }

    /**
     * 리스트 섞기
     */
    public static List<String> shuffle(List<String> strings) {
        for (int x = 0; x < strings.size(); x++) {
            int i = (int) (Math.random() * strings.size());
            int j = (int) (Math.random() * strings.size());

            String tmp = strings.get(i);
            strings.set(i, strings.get(j));
            strings.set(j, tmp);
        }

        return strings;
    }
}
