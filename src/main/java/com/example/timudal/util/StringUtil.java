package com.example.timudal.util;

public class StringUtil {



    public static boolean isNotNullEmpty(String s){
        return (s != null && !s.isEmpty());
    }

    public static boolean isNullEmpty(String s){
        if (s == null){
            return true;
        }
        return s.isEmpty();
    }
}
