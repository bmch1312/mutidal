package com.example.timudal.model;

import lombok.Data;

@Data
public class ChatMessage {

    private String content;
    private User user;
}
