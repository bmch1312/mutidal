package com.example.timudal.model;

public enum  MessageType {
    //초기관련
    JOIN,
    LEAVE,
    //채팅관련
    CHAT,
    //행동관련
    MAX_USER,
    MIN_USER,
    INIT,
    SELECT,
    SELECT_DONE,
    TRIBUTE_CONFIRM,
    TRIBUTE,
    TRIBUTE_DONE,
    PLAY,
    PASS,
    DEAL
}
