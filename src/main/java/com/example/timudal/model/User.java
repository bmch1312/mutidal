package com.example.timudal.model;

import lombok.Data;

@Data
public class User {

    private String userName;

    private String hostYn;
}
