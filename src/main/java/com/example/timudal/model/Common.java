package com.example.timudal.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Common {

    private int userCount = 0;

    private String hostUser = "";

    private String status = "WAIT";

    private int baseDeckSize = 80;

    private int turnCount = 0;

    private int nowTurn = 1;

    private int finishPlayerCount = 0;

    private int tributeCount = 0;

    private List<String> beforeDeal = new ArrayList<>();

    private List<String> baseDeck;

    private List<String> selectNumList = new ArrayList<>();

    private List<String> randomNumList;

    private List<Integer> initNumList = new ArrayList<>();

    private Map<Integer, List<String>> userDeckMap = new HashMap<>();

}
