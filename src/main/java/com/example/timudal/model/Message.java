package com.example.timudal.model;

import lombok.Data;

@Data
public class Message<T> {

    private MessageType messageType;

    private T data;

    private String status;

    private String msg;
}
