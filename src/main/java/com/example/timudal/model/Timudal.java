package com.example.timudal.model;

import lombok.Data;

import java.util.List;

@Data
public class Timudal {

    /** 유저이름*/
    private String userName;

    /** 고른 숫자*/
    private String selectNum;

    /** 맨처음 뽑은 숫자*/
    private String initNum;

    /** 내순위*/
    private int myRank;

    /** Deal Deck*/
    private List<String> dealDeckList;

    /** 덱*/
    private List<String> deckList;

    /** 현재 턴*/
    private int nowTurn;

    /** 종료된 사람 수*/
    private int finishPlayerCount;

    /** Change Deck*/
    private List<String> changeDeckList;

    private int tributeRank;

}
